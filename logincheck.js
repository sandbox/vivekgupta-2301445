/**
 * @file
 * Drupal site via AJAX to determine if the user
 * currently accessing the site still has an active session and reload the page
 * if they don not. Effectively logging the user out of the site.
 */
(function ($) {
  Drupal.behaviors.logincheck = {
    attach: function (context, settings) {
      Drupal.logincheck = {
        windowFocus: true,
        overdue: false
      };
      function logincheckCheck() {
        if (Drupal.logincheck.windowFocus) {
          $.get('/logincheck/check', function(data){
            // If the test returns 0 the user's session has ended so refresh the
            // page.
            if (data === '0') {
              window.location.reload(true);
            }
          });
        }
        else {
          Drupal.logincheck.overdue = true;
        }
      }
    }
  };

}(jQuery));
